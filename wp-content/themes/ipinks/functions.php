<?php
/**
 * Genesis Sample.
 *
 * This file adds functions to the Genesis Sample Theme.
 *
 * @package Genesis Sample
 * @author  StudioPress
 * @license GPL-2.0+
 * @link    https://www.studiopress.com/
 */

// Starts the engine.
require_once get_template_directory() . '/lib/init.php';

// Sets up the Theme.
require_once get_stylesheet_directory() . '/lib/theme-defaults.php';

add_action( 'after_setup_theme', 'genesis_sample_localization_setup' );
/**
 * Sets localization (do not remove).
 *
 * @since 1.0.0
 */
function genesis_sample_localization_setup() {

	load_child_theme_textdomain( 'genesis-sample', get_stylesheet_directory() . '/languages' );
	load_child_theme_textdomain( 'ipinks', get_stylesheet_directory() . '/languages' );	

}

// Adds helper functions.
require_once get_stylesheet_directory() . '/lib/helper-functions.php';

// Adds image upload and color select to Customizer.
require_once get_stylesheet_directory() . '/lib/customize.php';

// Includes Customizer CSS.
require_once get_stylesheet_directory() . '/lib/output.php';

// Adds WooCommerce support.
require_once get_stylesheet_directory() . '/lib/woocommerce/woocommerce-setup.php';

// Adds the required WooCommerce styles and Customizer CSS.
require_once get_stylesheet_directory() . '/lib/woocommerce/woocommerce-output.php';

// Adds the Genesis Connect WooCommerce notice.
require_once get_stylesheet_directory() . '/lib/woocommerce/woocommerce-notice.php';

// Defines the child theme (do not remove).
define( 'CHILD_THEME_NAME', 'Genesis Sample' );
define( 'CHILD_THEME_URL', 'https://www.studiopress.com/' );
define( 'CHILD_THEME_VERSION', '2.6.0' );

add_action( 'wp_enqueue_scripts', 'genesis_sample_enqueue_scripts_styles' );
/**
 * Enqueues scripts and styles.
 *
 * @since 1.0.0
 */
function genesis_sample_enqueue_scripts_styles() {

	wp_enqueue_style(
		'genesis-sample-fonts',
		'//fonts.googleapis.com/css?family=Source+Sans+Pro:400,400i,600,700',
		array(),
		CHILD_THEME_VERSION
	);
	wp_enqueue_style( 'dashicons' );

	$suffix = ( defined( 'SCRIPT_DEBUG' ) && SCRIPT_DEBUG ) ? '' : '.min';
	wp_enqueue_script(
		'genesis-sample-responsive-menu',
		get_stylesheet_directory_uri() . "/js/responsive-menus{$suffix}.js",
		array( 'jquery' ),
		CHILD_THEME_VERSION,
		true
	);
	wp_localize_script(
		'genesis-sample-responsive-menu',
		'genesis_responsive_menu',
		genesis_sample_responsive_menu_settings()
	);

	wp_enqueue_script(
		'genesis-sample',
		get_stylesheet_directory_uri() . '/js/genesis-sample.js',
		array( 'jquery' ),
		CHILD_THEME_VERSION,
		true
	);

}

/**
 * Defines responsive menu settings.
 *
 * @since 2.3.0
 */
function genesis_sample_responsive_menu_settings() {

	$settings = array(
		'mainMenu'         => __( 'Menu', 'genesis-sample' ),
		'menuIconClass'    => 'dashicons-before dashicons-menu',
		'subMenu'          => __( 'Submenu', 'genesis-sample' ),
		'subMenuIconClass' => 'dashicons-before dashicons-arrow-down-alt2',
		'menuClasses'      => array(
			'combine' => array(
				'.nav-primary',
			),
			'others'  => array(),
		),
	);

	return $settings;

}

// Sets the content width based on the theme's design and stylesheet.
if ( ! isset( $content_width ) ) {
	$content_width = 702; // Pixels.
}

// Adds support for HTML5 markup structure.
add_theme_support(
	'html5', array(
		'caption',
		'comment-form',
		'comment-list',
		'gallery',
		'search-form',
	)
);

// Adds support for accessibility.
add_theme_support(
	'genesis-accessibility', array(
		'404-page',
		'drop-down-menu',
		'headings',
		'rems',
		'search-form',
		'skip-links',
	)
);

// Adds viewport meta tag for mobile browsers.
add_theme_support(
	'genesis-responsive-viewport'
);

// Adds custom logo in Customizer > Site Identity.
add_theme_support(
	'custom-logo', array(
		'height'      => 120,
		'width'       => 700,
		'flex-height' => true,
		'flex-width'  => true,
	)
);

// Renames primary and secondary navigation menus.
add_theme_support(
	'genesis-menus', array(
		'primary'   => __( 'Header Menu', 'genesis-sample' ),
		'secondary' => __( 'Footer Menu', 'genesis-sample' ),
	)
);

// Adds support for after entry widget.
add_theme_support( 'genesis-after-entry-widget-area' );

// Adds support for 3-column footer widgets.
add_theme_support( 'genesis-footer-widgets', 3 );

// Removes header right widget area.
unregister_sidebar( 'header-right' );

// Removes secondary sidebar.
unregister_sidebar( 'sidebar-alt' );

// Removes site layouts.
genesis_unregister_layout( 'content-sidebar-sidebar' );
genesis_unregister_layout( 'sidebar-content-sidebar' );
genesis_unregister_layout( 'sidebar-sidebar-content' );

// Removes output of primary navigation right extras.
remove_filter( 'genesis_nav_items', 'genesis_nav_right', 10, 2 );
remove_filter( 'wp_nav_menu_items', 'genesis_nav_right', 10, 2 );

add_action( 'genesis_theme_settings_metaboxes', 'genesis_sample_remove_metaboxes' );
/**
 * Removes output of unused admin settings metaboxes.
 *
 * @since 2.6.0
 *
 * @param string $_genesis_admin_settings The admin screen to remove meta boxes from.
 */
function genesis_sample_remove_metaboxes( $_genesis_admin_settings ) {

	remove_meta_box( 'genesis-theme-settings-header', $_genesis_admin_settings, 'main' );
	remove_meta_box( 'genesis-theme-settings-nav', $_genesis_admin_settings, 'main' );

}

add_filter( 'genesis_customizer_theme_settings_config', 'genesis_sample_remove_customizer_settings' );
/**
 * Removes output of header settings in the Customizer.
 *
 * @since 2.6.0
 *
 * @param array $config Original Customizer items.
 * @return array Filtered Customizer items.
 */
function genesis_sample_remove_customizer_settings( $config ) {

	unset( $config['genesis']['sections']['genesis_header'] );
	return $config;

}

// Displays custom logo.
add_action( 'genesis_site_title', 'the_custom_logo', 0 );

// Repositions primary navigation menu.
remove_action( 'genesis_after_header', 'genesis_do_nav' );
add_action( 'genesis_header', 'genesis_do_nav', 12 );

// Repositions the secondary navigation menu.
remove_action( 'genesis_after_header', 'genesis_do_subnav' );
add_action( 'genesis_footer', 'genesis_do_subnav', 10 );

add_filter( 'wp_nav_menu_args', 'genesis_sample_secondary_menu_args' );
/**
 * Reduces secondary navigation menu to one level depth.
 *
 * @since 2.2.3
 *
 * @param array $args Original menu options.
 * @return array Menu options with depth set to 1.
 */
function genesis_sample_secondary_menu_args( $args ) {

	if ( 'secondary' !== $args['theme_location'] ) {
		return $args;
	}

	$args['depth'] = 1;
	return $args;

}

add_filter( 'genesis_author_box_gravatar_size', 'genesis_sample_author_box_gravatar' );
/**
 * Modifies size of the Gravatar in the author box.
 *
 * @since 2.2.3
 *
 * @param int $size Original icon size.
 * @return int Modified icon size.
 */
function genesis_sample_author_box_gravatar( $size ) {

	return 90;

}

add_filter( 'genesis_comment_list_args', 'genesis_sample_comments_gravatar' );
/**
 * Modifies size of the Gravatar in the entry comments.
 *
 * @since 2.2.3
 *
 * @param array $args Gravatar settings.
 * @return array Gravatar settings with modified size.
 */
function genesis_sample_comments_gravatar( $args ) {

	$args['avatar_size'] = 60;
	return $args;

}

add_theme_support( 'genesis-footer-widgets', 5 );
/* add custom script file */
function my_assets() {
	wp_enqueue_script( 'bootstrap-script', get_stylesheet_directory_uri() . '/bootstrap/css/bootstrap.min.js' );
	wp_enqueue_style( 'bootstrap-style', get_stylesheet_directory_uri() . '/bootstrap/css/bootstrap.min.css');
	wp_enqueue_style( 'fontawesome-style', get_stylesheet_directory_uri() . '/font-awesome/css/font-awesome.min.css');
}
add_action( 'wp_enqueue_scripts', 'my_assets' );
function my_assets_priority() {
	wp_enqueue_script( 'custom-script', get_stylesheet_directory_uri() . '/custom.js' );
	wp_enqueue_style( 'custom-stylesheet', get_stylesheet_directory_uri() . '/custom.css');
}
add_action( 'wp_enqueue_scripts', 'my_assets_priority',10 );
genesis_register_sidebar( array(
	'id' => 'superheader',
	'name' => __( 'Superheader', 'genesis' ),
	'description' => __( 'Superheader Menu Area', 'childtheme' ),
) );
add_action( 'genesis_header', 'add_genesis_superheader_menu_area',5 );

function add_genesis_superheader_menu_area() {
	genesis_widget_area( 'superheader', array(
		'before' => '<div class="superheader widget-area"><div>',
		'after'  => '</div></div>',
    ) );
}

add_action( 'genesis_after_header', 'add_searchbox' );

function add_searchbox() {
echo get_search_form();
}
add_action( 'get_header', 'remove_titles_all_single_pages' );
function is_post_type($type){
    global $wp_query;
    if($type == get_post_type($wp_query->post->ID)) return true;
    return false;
}
function custom_title(){
	global $post;
	$postcat = get_the_category( $post->ID );
	if ( ! empty( $postcat ) ) {
		echo "<a href='".get_permalink(13)."' class='back'>".__("&laquo; Indietro","ipinks")."</a>";
		echo "<h2>".esc_html( $postcat[0]->name )."</h2>";   
	}
}
function remove_titles_all_single_pages() {
    if ( !is_single()||is_post_type("prodotti") ) {
        remove_action( 'genesis_entry_header', 'genesis_do_post_title' );
	}else{
		add_action( 'genesis_entry_header', 'custom_title', 9 );
	}
}
function download( $atts ) {
	$content='<div class="row download">';    
    if( have_rows('singolo_elemento') ):
		while ( have_rows( 'singolo_elemento' ) ): the_row();
			$content.= '<div class="col-xs-6 col-sm-3">';
			$content.= "<figure style='background-image:url(".get_sub_field( 'immagine' ).");'><img src='" . get_sub_field( 'immagine' ) . "'></figure>";
			$content.= "<h3>". get_sub_field( 'titolo_certificazione' )."</h3>";
			$content.= "<p>". get_sub_field( 'testo_certificazione' )."</p>";
			$content.= "<span class='download_link'><a href='" . get_sub_field( 'link' ) . "' target='_blank'>" . get_sub_field( 'testo' ) . "</a></span>";
			$content.= "</div>";
	endwhile;
	endif;
	$content.='</div>';
    return $content;
}
add_shortcode( 'download', 'download');
function team( $atts ) {
	$content='<div class="row team"><div>';    
        $loop = new WP_Query( array( 'post_type' => 'team', 'posts_per_page'=>-1 ) );
		if ( $loop->have_posts() ) :
			while ( $loop->have_posts() ) : $loop->the_post();	
				$content.='<div class="col-xs-6 col-md-4"><figure>';    
				$content.='<img src="'.get_the_post_thumbnail_url().'">';
				$content.='<figcaption><b>'.get_the_title().'</b><span>'.get_the_content().'</span></figcaption>';
				$content.='</figure></div>';    
			endwhile;
    	endif;
		wp_reset_postdata();
	$content.="</div></div><div class='col-md-12 navigation_mobile'><a href='' class='navigation_arrow left'><i class='fa fa-angle-left' aria-hidden='true'></i></a><a href='' class='navigation_arrow right'><i class='fa fa-angle-right' aria-hidden='true'></i></a></div>";
    return $content;
}
add_shortcode( 'team', 'team');

function articoli( $atts ) {
	$content='<div class="categories_blog">';
	$content.='<ul>';
	$this_url=get_the_permalink();
	session_start();
	$categories = get_categories( array('orderby' => 'name','order'   => 'ASC') );
	if(($_COOKIE['hash']=="all")||(!isset($_COOKIE['hash'])))
			$class="active";
	$content.= "<li><a href='".$current_page."' class='category_hash ".$class."' data-slug='all'>".__("Tutti","inled")."</a></li>";
	foreach( $categories as $category ) {
		$class="";
		if($_COOKIE['hash']==$category->slug)
			$class="active";
		$content.= "<li><a href='".$current_page."#".$category->slug."' class='category_hash ".$class."' data-slug='".$category->slug."'>".ucfirst (strtolower($category->name))."</a></li>";
	} 
	$content.='</ul>';
	$content.='<a href="#" class="open_categories"><img src="/wp-content/themes/ipinks/images/arrow_down.png" ></a></div>';
	$content.='<main class="blog_container">';
	global $post;
//	// arguments, adjust as needed
	if(($_COOKIE['hash']=="")||($_COOKIE['hash']=="all")||(!isset($_COOKIE['hash'])))
	$args = array(
		'posts_per_page' => -1,
		'post_status'    => 'publish',
	);
	else
		$args = array(
		'posts_per_page' => -1,
		'post_status'    => 'publish',
		'category_name'  => $_COOKIE['hash']//,
		//'category__not_in' => array( 3 )
	);
	global $post;
	$wp_query = new WP_Query( $args );
	if ( $wp_query->have_posts() ) : 
		$content.= "<div class='blog_container'>";
			$content.='<div class="row">';
			while ($wp_query->have_posts() ) : $wp_query->the_post();
				
				$category="";
				$category_detail=get_the_category();//$post->ID
				foreach($category_detail as $cd){
					$category= $cd->cat_name;
				}
		    	$content_article = get_the_content(); 
				$content_article = mb_strimwidth($content_article, 0, 100, '...');
			    $content.='<div class="col-xs-6 col-md-3">
								<figure style="background-image:url('.get_the_post_thumbnail_url().')">
									<a href="'.get_the_permalink().'"><img src="'.get_the_post_thumbnail_url().'">
									
										
										<figcaption>
										<span>'.$category.'</span>
										<b>'.get_the_title().'</b>
									</figcaption>
									</a>
						   		<figure>
						   </div>';
			endwhile; 
		$content.= "</div>";
		$content.= "<a href='#' class='load_more'>".__('Carica','')."</a>";
		wp_reset_postdata(); 
	endif; 
			$content.='</div></main>
			<script>
				(function($) {
					$( document ).ready(function() {
						$("a.category_hash").click(function(e){
							e.preventDefault();
							document.cookie = "hash="+$(this).attr(\'data-slug\')+"; expires=0; path=/";
							window.location.href="'.$this_url.'";
						});
						$("a.open_categories").click(function(e){
							e.preventDefault();
							if(!$(".categories_blog ul").hasClass("open")){
								$(".categories_blog ul").css("height","100%");
								$(".categories_blog ul").addClass("open");
							}else{
								$(".categories_blog ul").css("height","31px");
								$(".categories_blog ul").removeClass("open");
							}
						});
						$(".blog_container>.row>div").each(function(){
							if($(this).css("display")=="block"){
								$(this).addClass("visible");										
							}								
						});
						if($(".blog_container>.row>div.visible").length==$(".blog_container>.row>div").length) $("a.load_more").fadeOut(300);
						$("a.load_more").click(function(e){
							e.preventDefault();
							var count=0;
							$(".blog_container>.row>div").each(function(){
								if(count<4){
									if($(this).css("display")=="none"){
										$(this).css("display","block");
										$(this).addClass("visible");										
										count++;
									}else{
										$(this).addClass("visible");
									}
								}								
							});
							if($(".blog_container>.row>div.visible").length==$(".blog_container>.row>div").length) $("a.load_more").fadeOut(300);
						});
					});
				})(jQuery);
			</script>';
	
	
	
	
	return $content;
}
add_shortcode( 'articoli', 'articoli');

function articoli_home( $atts ) {
	$content='<div class="categories_blog_home">';
	global $post;
//	// arguments, adjust as needed
	if(($_COOKIE['hash']=="")||($_COOKIE['hash']=="all")||(!isset($_COOKIE['hash'])))
	$args = array(
		'posts_per_page' => 4,
		'post_status'    => 'publish',
	);
	else
		$args = array(
		'posts_per_page' => -1,
		'post_status'    => 'publish',
		'category_name'  => $_COOKIE['hash']//,
		//'category__not_in' => array( 3 )
	);
	global $post;
	$wp_query = new WP_Query( $args );
	if ( $wp_query->have_posts() ) : 
			$content.='<div class="row">';
			while ($wp_query->have_posts() ) : $wp_query->the_post();
				
				$category="";
				$category_detail=get_the_category();//$post->ID
				foreach($category_detail as $cd){
					$category= $cd->cat_name;
				}
		    	$content_article = get_the_content(); 
				$content_article = mb_strimwidth($content_article, 0, 100, '...');
			    $content.='<div class="col-sm-6 col-md-4">
								<figure style="background-image:url('.get_the_post_thumbnail_url().')">
									<a href="'.get_the_permalink().'"><img src="'.get_the_post_thumbnail_url().'">
										<figcaption>
										<span>'.$category.'</span>
										<b>'.get_the_title().'</b>
									</figcaption>
									</a>
						   		<figure>
						   </div>';
			endwhile; 
		$content.= "</div>";
		wp_reset_postdata(); 
	endif; 
			$content.='</div>';
			
	return $content;
}
add_shortcode( 'articoli_home', 'articoli_home');

function blocchi_home_page( $atts ) {
	$content='<div class="row blocchi_home">';  
	$count=0;
	while ( have_rows( 'campi' ) ): the_row();
			if(($count==1)||($count==2))
				$content.= '<div class="col-xs-3 col-md-2"><div>';
			else
				$content.= '<div class="col-xs-3 col-md-4"><div>';
			$content.= "<h5>". get_sub_field( 'titolo_grande' )."</h5>";
			$content.= "<h6>". get_sub_field( 'titolo_piccolo' )."</h6>";
			$content.= "</div></div>";
			$count++;
	endwhile;
	$content.='</div>';
    return $content;
}
add_shortcode( 'blocchi_home_page', 'blocchi_home_page');

add_action( 'genesis_after_header', 'featured_post_image', 11 );
function featured_post_image() {
  if ( is_single() ) {
	  if (get_field( "immagine_in_evidenza" ) ){
		  echo "<div class='featured_image' style='background-image:url(".get_field( "immagine_in_evidenza" ).");'></div>";
	  }
	}
}





function prodotti( $atts ) {
	$content='<div class="categories_blog prodotti">';
	$content.='<ul>';
	$this_url=get_the_permalink();
	session_start();
	$categories = get_terms( 'categoria_prodotti', array('hide_empty' => true , 'parent' => 0) );

	if(($_COOKIE['hash']=="all")||(!isset($_COOKIE['hash'])))
			$class="active";
	$content.= "<li><a href='".$current_page."' class='category_hash ".$class."' data-slug='all'>".__("Tutti","ipinks")."</a></li>";
	foreach( $categories as $category ) {
		$class="";
		if($_COOKIE['hash']==$category->term_id)
			$class="active";
		$content.= "<li><a href='".$current_page."#".$category->slug."' class='category_hash ".$class."' data-slug='".$category->term_id."'>".$category->name."</a></li>";
	} 
	$content.='</ul>';
	$content.='<a href="#" class="open_categories"><img src="/wp-content/themes/ipinks/images/arrow_down.png" ></a></div>';
	$content.='<main class="blog_container">';
	global $post;
//	// arguments, adjust as needed
	if(($_COOKIE['hash']=="")||($_COOKIE['hash']=="all")||(!isset($_COOKIE['hash'])))
	$args = array(
		'posts_per_page' => -1,
		'post_status'    => 'publish',
		'post_type'=> 'prodotti'
	);
	else
		$args = array(
		'posts_per_page' => -1,
		'post_status'    => 'publish',
		'post_type'=> 'prodotti',
		'tax_query' => array(
				array(
					'taxonomy' => 'categoria_prodotti',
					'field'    => 'id',
					'terms'    => $_COOKIE['hash'],
					'include_children' => false
				),),//,
		//'category__not_in' => array( 3 )
	);
	global $post;
	$wp_query = new WP_Query( $args );
	if ( $wp_query->have_posts() ) : 
		$content.= "<div class='blog_container'>";
			$content.='<div class="row">';
			while ($wp_query->have_posts() ) : $wp_query->the_post();
				
				$category="";
				$category_detail=get_the_terms( get_the_ID(), 'categoria_prodotti' );//$post->ID
				foreach($category_detail as $cd){
					$category= $cd->slug;
				}
		    	$content_article = get_the_content(); 
				$content_article = mb_strimwidth($content_article, 0, 100, '...');
			    $content.='<div class="col-xs-12 col-md-3">
								<figure style="background-image:url('.get_the_post_thumbnail_url().')">
									<a href="'.get_the_permalink().'"><img src="'.get_the_post_thumbnail_url().'">
									
										
										<figcaption>
										<div><span>'.$category.'</span>
										<b>'.get_the_title().'</b></div>
									</figcaption>
									</a>
						   		<figure>
						   </div>';
			endwhile; 
		$content.= "</div>";
		$content.= "<a href='#' class='load_more'>".__('Load more','ipinks')."</a>";
		wp_reset_postdata(); 
	endif; 
			$content.='</div></main>
			<script>
				(function($) {
					$( document ).ready(function() {
						$("a.category_hash").click(function(e){
							e.preventDefault();
							document.cookie = "hash="+$(this).attr(\'data-slug\')+"; expires=0; path=/";
							window.location.href="'.$this_url.'";
						});
						$("a.open_categories").click(function(e){
							e.preventDefault();
							if(!$(".categories_blog ul").hasClass("open")){
								$(".categories_blog ul").css("height","100%");
								$(".categories_blog ul").addClass("open");
							}else{
								$(".categories_blog ul").css("height","31px");
								$(".categories_blog ul").removeClass("open");
							}
						});
						$(".blog_container>.row>div").each(function(){
							if($(this).css("display")=="block"){
								$(this).addClass("visible");										
							}								
						});
						if($(".blog_container>.row>div.visible").length==$(".blog_container>.row>div").length) $("a.load_more").fadeOut(300);
						$("a.load_more").click(function(e){
							e.preventDefault();
							var count=0;
							$(".blog_container>.row>div").each(function(){
								if(count<4){
									if($(this).css("display")=="none"){
										$(this).css("display","block");
										$(this).addClass("visible");										
										count++;
									}else{
										$(this).addClass("visible");
									}
								}								
							});
							if($(".blog_container>.row>div.visible").length==$(".blog_container>.row>div").length) $("a.load_more").fadeOut(300);
						});
					});
				})(jQuery);
			</script>';
	
	
	
	
	return $content;
}
add_shortcode( 'prodotti', 'prodotti');