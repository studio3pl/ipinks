(function($) {
	$( document ).ready(function() {
		$.noConflict();
		$(".genesis-nav-menu li.fa-search a").on({
			"click":function() {
				if($(".site-container .search-form").css("display")=="none"){
					$(".site-container .search-form").slideDown(300);
				}else{
					$(".site-container .search-form").slideUp(300);
				}
			}
		});
		function init_sliderMobileteam(){
			if($(window).width()<992){
				$(".team .col-xs-6.col-md-4").outerWidth($(".team").innerWidth()/2);
				var count=0;
				$(".team .col-xs-6.col-md-4").each(function(){
					if(count==0){
						$(this).attr("data-first","active");
						count++;
					}
				});
				$(".team>div").height($(".team .col-xs-6.col-md-4[data-first=active]").outerHeight());	
				$(".team>div").width($(".team .col-xs-6.col-md-4").width()*($(".team .col-xs-6.col-md-4").length+1));	
				$(".team").css("visibility","visible");

			}else{
				$(".team .col-xs-6.col-md-4").removeAttr("style");
				$(".team>div").removeAttr("style");
				$(".team .col-xs-6.col-md-4").removeAttr("style");
			}
		}
		function init_tab_dark(){
			if($(window).width()<992){
				if($(window).width()<600){
					$(".tab_light .vc_tta-tabs .vc_tta-tabs-list .vc_tta-tab, .tab_dark .vc_tta-tabs .vc_tta-tabs-list .vc_tta-tab").width(($(window).width()/2)-15);
				}
				else{
					$(".tab_light .vc_tta-tabs .vc_tta-tabs-list .vc_tta-tab, .tab_dark .vc_tta-tabs .vc_tta-tabs-list .vc_tta-tab").width(($(window).width()/3)-15);
				}
				var $length=0;
				$(".tab_dark .vc_tta-tabs .vc_tta-tabs-list .vc_tta-tab").each(function(){
					$length+=$(this).width();
				});
				$(".tab_dark .vc_tta-tabs .vc_tta-tabs-list").width($length);
				$length=0;
				$(".tab_light .vc_tta-tabs .vc_tta-tabs-list .vc_tta-tab").each(function(){
					$length+=$(this).width();
				});
				$(".tab_light .vc_tta-tabs .vc_tta-tabs-list").width($length);
				if($(".next_tab").length==0){
					$(".tab_light .vc_tta-tabs .vc_tta-tabs-container, .tab_dark .vc_tta-tabs .vc_tta-tabs-container").prepend('<a class="prev_tab"><i class="fa fa-angle-left" aria-hidden="true"></i></a><a class="next_tab"><i class="fa fa-angle-right" aria-hidden="true"></i></a>');
					$("a.next_tab").click(
						function(e){
							//alert($(this).next().find(".vc_tta-tab:first-child").offset().left);
							//alert($(this).next().find(".vc_tta-tab").length-1);
							if($(window).width()<600){
								if(($(this).next().find(".vc_tta-tab:first-child").offset().left*(-1))<$(this).next().find(".vc_tta-tab").width()*($(this).next().find(".vc_tta-tab").length-2)){

									$(this).next().find(".vc_tta-tab").stop().animate({"left":"-="+$(this).next().find(".vc_tta-tab").width()+"px"});
								}
							}else{
								if(($(this).next().find(".vc_tta-tab:first-child").offset().left*(-1))<$(this).next().find(".vc_tta-tab").width()*($(this).next().find(".vc_tta-tab").length-3)){

									$(this).next().find(".vc_tta-tab").stop().animate({"left":"-="+$(this).next().find(".vc_tta-tab").width()+"px"});
								}
							}
					});
					$("a.prev_tab").click(
						function(e){
							//alert($(this).next().find(".vc_tta-tab:first-child").offset().left);
							//alert($(this).next().find(".vc_tta-tab").length-1);
							if($(window).width()<600){
								if(($(this).next().next().find(".vc_tta-tab:first-child").offset().left)<0){

									$(this).next().next().find(".vc_tta-tab").stop().animate({"left":"+="+$(this).next().next().find(".vc_tta-tab").width()+"px"});
								}
							}else{
								if(($(this).next().next().find(".vc_tta-tab:first-child").offset().left)<0){

									$(this).next().next().find(".vc_tta-tab").stop().animate({"left":"+="+$(this).next().next().find(".vc_tta-tab").width()+"px"});
								}
							}
					});
				}
			}else{
				$(".tab_light .vc_tta-tabs .vc_tta-tabs-list").removeAttr("style");
				$(".tab_dark .vc_tta-tabs .vc_tta-tabs-list").removeAttr("style");
				$(".tab_light .vc_tta-tabs .vc_tta-tabs-list .vc_tta-tab, .tab_dark .vc_tta-tabs .vc_tta-tabs-list .vc_tta-tab").removeAttr("style");
				$(".next_tab, .prev_tab").remove();
				$(".single-prodotti .vc_tta-tab").css("left","0px");
			}
		}
		$(window).on({
			 "load resize":function(){
				init_sliderMobileteam();
				init_tab_dark();
			}
		});
		
		$(".chi-siamo .navigation_mobile .navigation_arrow").click(function(e){
			e.preventDefault();
			if($(this).hasClass("right")){
				var first=$(".team .col-xs-6.col-md-4:nth-child(2)");
				var last=$(".team .col-xs-6.col-md-4[data-first=active]");
				$(".team .col-xs-6.col-md-4").stop().animate({"left":"-="+$(".team .col-xs-6.col-md-4").width()+"px"},500,function(){
					$(".team .col-xs-6.col-md-4").css("left","0");
					$(".team>div").append(last.removeAttr("data-first"));
				});				
				first.attr("data-first","active");
			}else{
				var first=$(".team .col-xs-6.col-md-4:last-child");
				var last=$(".team .col-xs-6.col-md-4[data-first=active]");
				$(".team .col-xs-6.col-md-4").css("left","-"+$(".team .col-xs-6.col-md-4").width()+"px");
				$(".team>div").prepend(first);
				last.removeAttr("data-first");
				$(".team .col-xs-6.col-md-4").stop().animate({"left":"+="+$(".team .col-xs-6.col-md-4").width()+"px"},500,function(){
					$(".team .col-xs-6.col-md-4").css("left","0");						
				});				
				first.attr("data-first","active");
			}
		});
	});
})(jQuery);// JavaScript Document