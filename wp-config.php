<?php
define('WP_CACHE', true);
/**
 * Il file base di configurazione di WordPress.
 *
 * Questo file viene utilizzato, durante l’installazione, dallo script
 * di creazione di wp-config.php. Non è necessario utilizzarlo solo via
 * web, è anche possibile copiare questo file in «wp-config.php» e
 * riempire i valori corretti.
 *
 * Questo file definisce le seguenti configurazioni:
 *
 * * Impostazioni MySQL
 * * Prefisso Tabella
 * * Chiavi Segrete
 * * ABSPATH
 *
 * È possibile trovare ultetriori informazioni visitando la pagina del Codex:
 *
 * @link https://codex.wordpress.org/it:Modificare_wp-config.php
 *
 * È possibile ottenere le impostazioni per MySQL dal proprio fornitore di hosting.
 *
 * @package WordPress
 */

// ** Impostazioni MySQL - È possibile ottenere queste informazioni dal proprio fornitore di hosting ** //
/** Il nome del database di WordPress */
define('DB_NAME', 'tdwdevit_ipinks');

/** Nome utente del database MySQL */
define('DB_USER', 'tdwdevit_ipinks');

/** Password del database MySQL */
define('DB_PASSWORD', '5rOt9ZRDi9Vo');

/** Hostname MySQL  */
define('DB_HOST', 'localhost');

/** Charset del Database da utilizzare nella creazione delle tabelle. */
define('DB_CHARSET', 'utf8mb4');

/** Il tipo di Collazione del Database. Da non modificare se non si ha idea di cosa sia. */
define('DB_COLLATE', '');

/**#@+
 * Chiavi Univoche di Autenticazione e di Salatura.
 *
 * Modificarle con frasi univoche differenti!
 * È possibile generare tali chiavi utilizzando {@link https://api.wordpress.org/secret-key/1.1/salt/ servizio di chiavi-segrete di WordPress.org}
 * È possibile cambiare queste chiavi in qualsiasi momento, per invalidare tuttii cookie esistenti. Ciò forzerà tutti gli utenti ad effettuare nuovamente il login.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         '<VO7<pSoBYMj4yKuT0)D;bfj|5_0y{]8Dkj7Y%|?~xLMPy(X8.5(87 :}?Yz!I&;');
define('SECURE_AUTH_KEY',  'HAhFZ@(I[M=`vfVU@5U|>5d|VV{x^mPC<{8hp!p-Kpnyv|[c+J=N1?u!}}k4mgs5');
define('LOGGED_IN_KEY',    '~?UexhKw:+hM&}A3R,EM5E^5S6K3~%xLk8<IzSf1q^@gQs+_<jSJ>6hO9+DU@Gd9');
define('NONCE_KEY',        '4>cq2;qB/!:l,0+{U6<d!ht!- |nmy)h!@WptJB tSs7?/W~A}*s{bYVo9uTKURv');
define('AUTH_SALT',        'cfgvU~XN|Yu|GN$$btsN2|v(O&[|T_Y4r3aP,?=g_AR5`]zee3qMsIUu%dbqZz{y');
define('SECURE_AUTH_SALT', '~2{T(*QQV42!_UN+n6`Q/~XC07e<R2:,TGokF$@FBM9If%:`SL>b$W><W`rX{2#+');
define('LOGGED_IN_SALT',   '=,$US;7-u?}.wp76cq8c]}%*cM&DPjs^jCcK<mw~iM_D06F#S@K~[[mkH>Ot:|Mz');
define('NONCE_SALT',       '#g()m!-F<`V<*r.XZT(%_+Y`bCxRr{0PyZzk(S()b|/2dt_w+<aX@-LT2@y.<($d');

/**#@-*/

/**
 * Prefisso Tabella del Database WordPress.
 *
 * È possibile avere installazioni multiple su di un unico database
 * fornendo a ciascuna installazione un prefisso univoco.
 * Solo numeri, lettere e sottolineatura!
 */
$table_prefix  = 'ipi_';

/**
 * Per gli sviluppatori: modalità di debug di WordPress.
 *
 * Modificare questa voce a TRUE per abilitare la visualizzazione degli avvisi
 * durante lo sviluppo.
 * È fortemente raccomandato agli svilupaptori di temi e plugin di utilizare
 * WP_DEBUG all’interno dei loro ambienti di sviluppo.
 */
define('WP_DEBUG', false);

/* Finito, interrompere le modifiche! Buon blogging. */

/** Path assoluto alla directory di WordPress. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Imposta le variabili di WordPress ed include i file. */
require_once(ABSPATH . 'wp-settings.php');